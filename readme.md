# AOA

Web application using Laravel 5.5.*  


## Install

This application can be cloned from gitlab repository and installed. Following the procedure given below:

* git clone `git@gitlab.com:maras2/aoa.git`
* cd aoa

## Run

The app can be run with the command below:

* install the application dependencies using command: `composer install`
* Copy .env file `cp .env.example .env`
* Update database and email configuration
* Migrate database using command: `php artisan migrate`
* Seed data to database using command: `php artisan db:seed`
* Configure your virtual host for the project eg `aoa.dev`
* access `http://aoa.dev`

## Working & Deploying
* Follow the concept of simplified gitflow.

## Framework

The application is written in PHP based on the [Laravel](http://laravel.com) framework, current version of Laravel 
used for this project is 5.5.7
 
## Running Tests

Tests can be run through command `./vendor/bin/phpunit`

## Coding Conventions

We have followed PSR-2 coding convention.
