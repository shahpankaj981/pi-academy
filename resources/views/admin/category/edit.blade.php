@extends('admin.layouts.master')
@php($title='Edit category')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('category.index')],
    ['title'=>'Edit','url'=>route('category.edit', $category->id)]
    ]
    ])
    category
    @endpageheader
@endsection
@section('content')
    @box([
    'form'=>['route' => ['category.update', $category->id], 'method' => 'patch', 'files' => true],
    'col'=>'md-12',
    'title'=>'Update Category',
    'model'=>$category,
    'content'=>[
    'view'=>'admin.category._form',
    'data'=>['formTitle'=>'Update Category']
    ]
    ])
    @endbox
@endsection

