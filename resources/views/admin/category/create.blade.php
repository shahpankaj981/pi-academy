@extends('admin.layouts.master')
@php($title='Create Category')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('category.index')],
    ['title'=>'Create','url'=>route('category.create')]
    ]
    ])
    Category
    @endpageheader
@endsection
@section('content')
    @box(['form' => ['route' => 'category.store', 'method' => 'post', 'files' => true],'col'=>'md-12'])
    @include('admin.category._form')
    @endbox
@endsection
