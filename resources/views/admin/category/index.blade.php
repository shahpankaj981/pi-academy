@extends('admin.layouts.master')
@php($title='List Category')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('category.index')]
    ]
    ])
    Category
    @endpageheader
@endsection
@section('content')
    @box(['title' => 'List', 'col'=>'md-12'])
    @table(['addNew'=>route('category.create')])
    <thead>
    <tr>
        <th>SN.</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td width="5%">{{ $loop->iteration }}</td>
            <td>{{ $category->name }}</td>
            <td width="5%">
                @action(['route'=>'category','id'=>$category->id, 'edit' => true, 'delete' => true])
                @endaction
            </td>
        </tr>
    @endforeach()
    </tbody>
    @endtable
    @endbox
@endsection
