@php
    $errors->setFormat('<span class="form-control-feedback">:message</span>')
@endphp
<div class="form-group m-form__group">
    <label for="name">Name *</label>
    {!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder'=>'Name']) !!}
    {!! $errors->first('name') !!}
</div>
<div class="form-group m-form__group">
    <label for="name">Order</label>
    {!! Form::number('order', null, ['class' => 'form-control m-input', 'min'=>1, 'placeholder'=>'Order']) !!}
    {!! $errors->first('order') !!}
</div>
<div class="form-group m-form__group">
    <label for="description">Description</label>
    {!! Form::text('description', null, ['class' => 'form-control m-input', 'placeholder'=>'Description']) !!}
    {!! $errors->first('description') !!}
</div>
<div class="form-group m-form__group">
    <label for="image">Image</label>
    {!! Form::file('image', ['class' => 'form-control']) !!}
    {!! $errors->first('image') !!}
</div>
