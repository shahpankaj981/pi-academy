@extends('admin.layouts.master')
@php($title='Edit subcategory')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('subcategory.index')],
    ['title'=>'Edit','url'=>route('subcategory.edit', $subcategory->id)]
    ]
    ])
    subcategory
    @endpageheader
@endsection
@section('content')
    @box([
    'form'=>['route' => ['subcategory.update', $subcategory->id], 'method' => 'patch', 'files' => true],
    'col'=>'md-12',
    'title'=>'Update SubCategory',
    'model'=>$subcategory,
    'categories' => $categories,
    'content'=>[
    'view'=>'admin.subcategory._form',
    'data'=>['formTitle'=>'Update SubCategory']
    ]
    ])
    @endbox
@endsection

