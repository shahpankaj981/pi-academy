@extends('admin.layouts.master')
@php($title='List SubCategory')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('subcategory.index')]
    ]
    ])
    SubCategory
    @endpageheader
@endsection
@section('content')
    @box(['title' => 'List', 'col'=>'md-12'])
    @table(['addNew'=>route('subcategory.create')])
    <thead>
    <tr>
        <th>SN.</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($subcategories as $subcategory)
        <tr>
            <td width="5%">{{ $loop->iteration }}</td>
            <td>{{ $subcategory->name }}</td>
            <td width="5%">
                @action(['route'=>'subcategory','id'=>$subcategory->id, 'edit' => true, 'delete' => true])
                @endaction
            </td>
        </tr>
    @endforeach()
    </tbody>
    @endtable
    @endbox
@endsection
