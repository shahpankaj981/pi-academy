@extends('admin.layouts.master')
@php($title='Create SubCategory')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('subcategory.index')],
    ['title'=>'Create','url'=>route('subcategory.create')]
    ]
    ])
    SubCategory
    @endpageheader
@endsection
@section('content')
    @box(['form' => ['route' => 'subcategory.store', 'method' => 'post', 'files' => true],'col'=>'md-12'])
    @include('admin.subcategory._form')
    @endbox
@endsection
