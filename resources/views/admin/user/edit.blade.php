@extends('admin.layouts.master')
@php($title='Edit User')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('user.index')],
    ['title'=>'Edit','url'=>route('user.edit', $user->id)]
    ]
    ])
    User
    @endpageheader
@endsection
@section('content')
    @box([
    'form'=>['route' => ['user.update', $user->id], 'method' => 'patch'],
    'col'=>'md-6',
    'title'=>'Update User',
    'model'=>$user,
    'content'=>[
    'view'=>'admin.user._form',
    'data'=>['formTitle'=>'Update User']
    ]
    ])
    @endbox
@endsection

