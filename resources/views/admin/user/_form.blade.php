@php
    $errors->setFormat('<span class="form-control-feedback">:message</span>')
@endphp
<div class="form-group m-form__group">
    <label for="name">Name *</label>
    {!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder'=>'Name']) !!}
    {!! $errors->first('name') !!}
</div>
<div class="form-group m-form__group">
    <label for="email">Email *</label>
    {!! Form::email('email', null, ['class' => 'form-control m-input', 'placeholder'=>'Email']) !!}
    {!! $errors->first('email') !!}
</div>
<div class="form-group m-form__group">
    <label for="password">Password *</label>
    {!! Form::password('password', ['class' => 'form-control m-input', 'placeholder'=>'Password']) !!}
    {!! $errors->first('password') !!}
</div>
<div class="form-group m-form__group">
    <label for="confirm">Confirm Password</label>
    {!! Form::password('password_confirmation', ['class' => 'form-control m-input','placeholder'=>'Confirm Password']) !!}
    {!! $errors->first('password_confirmation') !!}
</div>
