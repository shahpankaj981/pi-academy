@extends('admin.layouts.master')
@php($title='List User')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('user.index')]
    ]
    ])
    User
    @endpageheader
@endsection
@section('content')
    @box(['title' => 'List', 'col'=>'md-12'])
    @table(['addNew'=>route('user.create')])
    <thead>
    <tr>
        <th>SN.</th>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td width="5%">{{ $loop->iteration }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td width="5%">
                @action(['route'=>'user','id'=>$user->id, 'edit' => true, 'delete' => true])
                @endaction
            </td>
        </tr>
    @endforeach()
    </tbody>
    @endtable
    @endbox
@endsection
