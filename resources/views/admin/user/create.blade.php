@extends('admin.layouts.master')
@php($title='Create User')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('user.index')],
    ['title'=>'Create','url'=>route('user.create')]
    ]
    ])
    User
    @endpageheader
@endsection
@section('content')
    @box(['form' => ['route' => 'user.store', 'method' => 'post'],'col'=>'md-6'])
    @include('admin.user._form')
    @endbox
@endsection
