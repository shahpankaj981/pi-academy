@extends('admin.layouts.master')
@php($title='List Email Template')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('email-template.index')]
    ]
    ])
    Email Template
    @endpageheader
@endsection
@section('content')
    @box(['title' => 'List', 'col'=>'md-12'])
    @table(['addNew'=>route('email-template.create')])
    <thead>
    <tr>
        <th>SN.</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($templates as $template)
        <tr>
            <td width="5%">{{ $loop->iteration }}</td>
            <td>{{ $template->title }}</td>
            <td width="5%">
                @action(['route'=>'email-template','id'=>$template->id, 'edit' => true, 'delete' => true])
                @endaction
            </td>
        </tr>
    @endforeach()
    </tbody>
    @endtable
    @endbox
@endsection
