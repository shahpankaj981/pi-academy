@extends('admin.layouts.master')
@php($title='Create Email Template')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('email-template.index')],
    ['title'=>'Create','url'=>route('email-template.create')]
    ]
    ])
    Email Template
    @endpageheader
@endsection
@section('content')
    @box(['form' => ['route' => 'email-template.store', 'method' => 'post'],'col'=>'md-6'])
    @include('admin.email-template._form')
    @endbox
@endsection
