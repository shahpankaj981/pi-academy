@php
    $errors->setFormat('<span class="form-control-feedback">:message</span>')
@endphp
<div class="form-group m-form__group">
    <label for="name">Title *</label>
    {!! Form::text('title', null, ['class' => 'form-control m-input', 'placeholder'=>'Title', 'onkeyup'=>'generateSlug(this.value)']) !!}
    {!! $errors->first('title') !!}
</div>
<div class="form-group m-form__group">
    <label for="slug">Slug *</label>
    {!! Form::text('slug', null, ['class' => 'form-control m-input', 'placeholder'=>'Slug', 'id'=>'slug']) !!}
    {!! $errors->first('slug') !!}
</div>
<div class="form-group m-form__group">
    <label for="content">Content *</label>
    {!! Form::textarea('content',null, ['class' => 'form-control m-input summernote']) !!}
    {!! $errors->first('content') !!}
</div>
@push('scripts')
    <script>
        function generateSlug(value){
            var slug =  value
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
            $('#slug').val(slug);
        }
        var content = function() {
            //== Private functions
            var demos = function() {
                $('.summernote').summernote({
                    height: 150
                });
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        //== Initialization
        jQuery(document).ready(function() {
            content.init();
        });
    </script>
@endpush
