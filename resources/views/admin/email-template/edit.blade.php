@extends('admin.layouts.master')
@php($title='Edit Email Template')
@section('header')
    @pageheader([
    'breadcrumbs'=>
    [
    ['title'=>'List','url'=>route('email-template.index')],
    ['title'=>'Edit','url'=>route('email-template.edit', $template->id)]
    ]
    ])
    Email Template
    @endpageheader
@endsection
@section('content')
    @box([
    'form'=>['route' => ['email-template.update', $template->id], 'method' => 'patch'],
    'col'=>'md-6',
    'title'=>'Edit Template Form',
    'model'=>$template,
    'content'=>[
    'view'=>'admin.email-template._form',
    'data'=>['formTitle'=>'Edit Email Template']
    ]
    ])
    @endbox
@endsection

