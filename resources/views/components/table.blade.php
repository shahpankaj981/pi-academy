@if (!empty($addNew))
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
                <a href="{{ $addNew }}" id="sample_editable_1_new"
                   class="btn m-btn--square  btn-outline-accent m-btn m-btn--custom m-btn--outline-2x"> Add New
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </div>
@else
    @if (isset($toolbar))
        <div class="table-toolbar">
            {!! $toolbar !!}
        </div>
    @endif
@endif
<table
    class="{{ !empty($tableClass) ? $tableClass:'table table-striped- table-bordered table-hover table-checkable' }}" {!! isset($id)? 'id="'.$id.'"':'' !!}>
    @if (isset($thead))
        {!! $thead !!}
    @endif
    @if (isset($tbody))
        {!! $tbody !!}
    @endif
    {{ $slot }}
</table>
