@if (!isset($wrapRow) )
    <div class="row">
@endif
    {!! $inject ?? '' !!}
    <div class="{{ !empty($col) ? 'col-'.$col : 'col-md-12' }}">
        <div class="m-portlet {{ $class ?? 'light bordered' }}">
            @if (!isset($showHead))
                <div class="m-portlet__head">
                    @if (!empty($head))
                        {{ $head }}
                    @else
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <i class="flaticon-technology m--font-accent"></i>&nbsp;
                                    {{$title ?? 'Create Form'}}
                                </h3>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
            @if (isset($form))
                @if (isset($model))
                    {!! Form::model($model, ['class'=>'m-form m-form--fit m-form--label-align-right']+$form) !!}
                @else
                    {!! Form::open(['class'=>'m-form m-form--fit m-form--label-align-right']+$form) !!}
                @endif
            @endif
            <div class="{{ isset($bodyClass) ? $bodyClass : 'm-portlet__body' }}">
                @if (isset($content))
                    @include($content['view'], $content['data'])
                @else
                    {{ $slot }}
                @endif
            </div>
            @if (!empty($footer))
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="reset" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            @endif
            @if (isset($form))
                <div class="m-portlet__foot m-portlet__foot--fit m--align-right">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@if (!isset($wrapRow))
    </div>
@endif
