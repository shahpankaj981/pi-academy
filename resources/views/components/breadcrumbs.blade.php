<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        {{ $slot }}
    </ul>
</div>
