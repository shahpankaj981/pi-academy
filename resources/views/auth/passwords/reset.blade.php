@extends('admin.layouts.app')
@php($loginClass='m-login--forget-password')
@section('content')
<div class="m-login__forget-password animated flipInX">
    <div class="m-login__head">
        <h3 class="m-login__title">
            Reset Password
        </h3>
    </div>
    <form class="m-login__form m-form" method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group m-form__group">
            <input  type="email" class="form-control m-input" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autofocus>
        </div>
        <div class="form-group m-form__group {{ $errors->has('password') ? 'has-danger' : '' }}">
            <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control m-input" name="password" required>
            @if ($errors->has('password'))
                <span class="form-control-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group m-form__group">
            <input id="password-confirm" type="password" class="form-control m-input" name="password_confirmation" placeholder="Confirm Password" required>
        </div>
        <div class="m-login__form-action">
            <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn--primaryr">
                {{ __('Reset Password') }}
            </button>
            &nbsp;&nbsp;
        </div>
    </form>
</div>
@endsection


