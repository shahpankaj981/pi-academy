@extends('admin.layouts.app')
@php($loginClass='m-login--forget-password')
@section('content')
    <div class="m-login__forget-password animated flipInX">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Forgotten Password ?
            </h3>
            <div class="m-login__desc">
                Enter your email to reset your password:
            </div>
        </div>
        <form class="m-login__form m-form" method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group m-form__group">
                <input type="email" class="form-control m-input" placeholder="{{ __('E-Mail Address') }}" name="email"
                       value="{{ old('email') }}" required autofocus>
            </div>
            <div class="m-login__form-action">
                <button type="submit"
                        class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn--primaryr">
                    {{ __('Send Password Reset Link') }}
                </button>
                &nbsp;&nbsp;
                <a href="{{ route('login') }}"
                   class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom ">
                    Cancel
                </a>
            </div>
        </form>
    </div>
@endsection
