@extends('admin.layouts.app')
@section('content')
    <div class="m-login__signin">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Sign In To Admin
            </h3>
        </div>
        <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group m-form__group {{ $errors->has('email') ? 'has-danger' : '' }}">
                <input id="email" type="email" class="form-control m-input {{ $errors->has('email') ? ' is-invalid' : '' }}"
                       placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="form-control-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group m-form__group {{ $errors->has('email') ? 'has-danger' : '' }}">
                <input id="password" type="password" placeholder="{{ __('Password') }}"
                       class="form-control m-input {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                       required>
                @if ($errors->has('password'))
                    <span class="form-control-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="row m-login__form-sub">
                <div class="col m--align-left m-login__form-left">
                    <label class="m-checkbox  m-checkbox--focus">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        Remember me
                        <span></span>
                    </label>
                </div>
                <div class="col m--align-right m-login__form-right">
                    <a href="{{ route('password.request') }}" class="m-link">
                        Forget Password ?
                    </a>
                </div>
            </div>
            <div class="m-login__form-action">
                <button type="submit" class="btn btn-outline-focus m-btn--pill">
                    Sign In
                </button>
            </div>
        </form>
    </div>
@endsection
