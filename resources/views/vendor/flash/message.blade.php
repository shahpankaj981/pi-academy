@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <div class="m-subheader">
            <div class="row">
                <div class="col-md-12">
                    <div class="m-alert m-alert--outline m-alert--outline-2x alert alert-{{ $message['level'] }} alert-dismissible fade show {{ $message['important'] ? 'alert-important' : '' }}" role="alert">
                        @if (!$message['important'])
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            </button>
                        @endif
                        {!! $message['message'] !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
