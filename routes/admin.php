<?php
$this->get('/home', 'HomeController@index')->name('home');
$this->resource('user', 'UserController');
$this->resource('category', 'CategoryController')->except(['show']);
$this->resource('subcategory', 'SubcategoryController')->except(['show']);
$this->resource('email-template', 'EmailTemplateController');
