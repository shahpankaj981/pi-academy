<?php

namespace App\Http\Controllers\Admin;

use App\Entities\EmailTemplate;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailTemplateRequest;
use App\Services\EmailTemplateService;
use Illuminate\Http\Request;

/**
 * Class EmailTemplateController
 * @package App\Http\Controllers\Admin
 */
class EmailTemplateController extends Controller
{
    /**
     * @var EmailTemplateService
     */
    private $emailTemplateService;
    /**
     * @var string
     */
    protected $redirectToIndex;

    /**
     * EmailTemplateController constructor.
     * @param EmailTemplateService $emailTemplateService
     */
    public function __construct(EmailTemplateService $emailTemplateService)
    {
        $this->redirectToIndex      = redirect()->route('email-template.index');
        $this->emailTemplateService = $emailTemplateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = $this->emailTemplateService->all();

        return view('admin.email-template.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.email-template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmailTemplateRequest $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function store(EmailTemplateRequest $request)
    {
        $this->emailTemplateService->create($request->all());
        flashCreated();

        return $this->redirectToIndex;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\EmailTemplate $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplate $emailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $template = $this->emailTemplateService->findOrFail($id);

        return view('admin.email-template.edit', compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmailTemplateRequest $request
     * @param int                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmailTemplateRequest $request, int $id)
    {
        $this->emailTemplateService->update($id, $request->all());
        flashUpdated();

        return $this->redirectToIndex;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $this->emailTemplateService->destroy($id);
        flashDeleted();

        return $this->redirectToIndex;
    }
}
