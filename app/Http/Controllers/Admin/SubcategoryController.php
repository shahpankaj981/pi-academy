<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Subcategory;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\SubcategoryRequest;
use App\Services\CategoryService;
use App\Services\SubcategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SubcategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $subcategoryService;
    protected $redirectToIndex;

    /**
     * CourseController constructor.
     * @param SubcategoryService $subcategoryService
     */
    public function __construct(SubcategoryService $subcategoryService)
    {
        $this->subcategoryService = $subcategoryService;
        $this->redirectToIndex    = redirect()->route('subcategory.index');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = $this->subcategoryService->all();

        return view('admin.subcategory.index', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = app(Category::class)->pluck('name', 'id');

        return view('admin.subcategory.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubcategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubcategoryRequest $request)
    {
        try {
            $this->subcategoryService->create($request);
            flashCreated();
        } catch (\Exception $exception) {
            logger('Error saving subcategory because: '.$exception->getMessage());
            flash('Something went wrong')->error();
        }

        return $this->redirectToIndex;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Subcategory $subcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {
        $categories = app(Category::class)->pluck('name', 'id');

        return view('admin.subcategory.edit', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SubcategoryRequest $request
     * @param Subcategory        $subcategory
     * @return \Illuminate\Http\Response
     */
    public function update(SubcategoryRequest $request, Subcategory $subcategory)
    {
        $this->subcategoryService->update($subcategory, $request);
        flashUpdated();

        return $this->redirectToIndex;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->subcategoryService->destroy($id);
        flashDeleted();

        return $this->redirectToIndex;
    }
}
