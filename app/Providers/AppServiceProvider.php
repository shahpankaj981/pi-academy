<?php

namespace App\Providers;

use App\Http\Services\Base\BaseInterface;
use App\Http\Services\Base\BaseService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.box', 'box');
        Blade::component('components.table', 'table');
        Blade::component('components.heading', 'heading');
        Blade::component('components.pageheader', 'pageheader');
        Blade::component('components.action', 'action');
        $this->app->bind(BaseService::class, BaseInterface::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
