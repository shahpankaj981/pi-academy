<?php

namespace App\Constants;

class MenuGroupConstant
{
    const GENERAL      = 1;
    const MASTER_ENTRY = 2;
}
