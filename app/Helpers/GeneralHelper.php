<?php

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

function findPageTitle()
{
    $subTitle = '';
    if (request()->segment(3)) {
        if ($title = request()->segment(4)) {
            $subTitle = $title;
        } else {
            $subTitle = request()->segment(3);
        }

    } else {
        $subTitle = 'List';
    }
    $title = sprintf("
<h1 class=\"page-title\">%s
    <small>%s</small>
</h1>", ucfirst(request()->segment(2)), $subTitle);

    echo $title;
}

function getFormattedUri()
{
    $uris     = array_filter(explode('/', request()->getRequestUri()));
    $all      = '';
    $count    = count($uris);
    $sequence = '';
    foreach ($uris as $key => $t) {
        $sequence .= "/$t";
        if ($t != 'admin' && $t) {
            if ($count != $key) {
                $m   = '<i class="fa fa-circle"></i>';
                $all .= sprintf("<li><a href=\"%s\">%s</a>%s</li>", $sequence, $t, $m);
            } else {
                $all .= "<li><span>$t</span></li>";
            }
        }
    }

    echo sprintf("<div class=\"page-bar\">
    <ul class=\"page-breadcrumb\">
        <li>
            <a href=\"/admin/dashboard\">Home</a>
            <i class=\"fa fa-circle\"></i>
        </li>
        %s
    </ul>
</div>", $all);
}

/**
 * @param null $message
 */
function flashUpdated($message = null)
{
    flash()->success($message ?? "Update Successfully");
}

/**
 * @param null $message
 */
function flashCreated($message = null)
{
    flash()->success($message ?? "Create Successfully");
}

/**
 * @param null $message
 */
function flashDuplicate($message = null)
{
    flash()->warning($message ?? "Duplicate Entry Found");
}

/**
 * @param null $message
 */
function flashDeleted($message = null)
{
    flash()->success($message ?? "Delete Successfully");
}

/**
 * store image on disk
 *
 * @param      $file
 * @param      $path
 * @param null $fileName
 * @return false|string
 */
function uploadImage($file, $path, $fileName = null)
{
    $fileName      = time().'.'.$file->getClientOriginalExtension();
    $savedFileName = $file->storeAs(
        $path, $fileName
    );
    Image::make($file)->resize(300, null, function ($constraint) {
        $constraint->aspectRatio();
    })->save();

    $file->storeAs(
        $path.'/thumbnail', $fileName
    );

    return $savedFileName;
}

function getToggleStatus($status, $whenTrue = 'Active', $whenFalse = 'Inactive')
{
    if ($status) {
        return '<span class="m-badge m-badge--success m-badge--wide">'.$whenTrue.'</span>';
    } else {
        return '<span class="m-badge m-badge--danger m-badge--wide">'.$whenFalse.'</span>';
    }
}

/**
 * @param      $file
 * @param bool $thumbnail
 * @return string
 */
function getFile($file, $thumbnail = false)
{
    if (!$file) {
        return asset('assets/images/blank.jpg');
    }
    if ($thumbnail) {
        $path  = explode('/', $file);
        $new   = ['thumbnail'];
        $count = count($path);
        array_splice($path, $count - 1, 0, $new);

        return Storage::url(implode('/', $path));
    }

    return Storage::url($file);
}

/**
 * @return mixed
 */
function menus()
{
    return app(\App\Services\AdminMenuService::class)->groupsWithMenus();
}
