<?php

namespace App\Services;


use App\Entities\EmailTemplate;
use App\Services\Base\BaseService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EmailTemplateService
 * @package App\Services
 */
class EmailTemplateService extends BaseService
{

    /**
     * @return Model
     */
    public function model()
    {
        return new EmailTemplate();
    }
}
