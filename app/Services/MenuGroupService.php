<?php

namespace App\Services;


use App\Entities\MenuGroup;
use App\Services\Base\BaseService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuGroupService
 * @package App\Services
 */
class MenuGroupService extends BaseService
{
    /**
     * @return Model
     */
    public function model()
    {
        return new MenuGroup();
    }
}
