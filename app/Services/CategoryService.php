<?php
/**
 * Created by PhpStorm.
 * User: pankaj
 * Date: 4/8/19
 * Time: 5:36 PM
 */

namespace App\Services;


use App\Entities\Category;
use App\Services\Base\BaseService;
use Illuminate\Database\Eloquent\Model;

class CategoryService extends BaseService
{
    /**
     * @return Model
     */
    public function model()
    {
        return new Category();
    }

    public function create($request)
    {
        $inputData = $request->all();
        if($request->has('image')) {
            $inputData['image'] = uploadImage($request['image'], 'images/categories');
        }

        return parent::create($inputData);
    }

    public function update($course, $request)
    {
        $updateData = $request->all();
        if($request->has('image')) {
            $updateData['image'] = uploadImage($request['image'], 'images/categories');
        }
        return $course->update($updateData);
    }
}
