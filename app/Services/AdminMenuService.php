<?php

namespace App\Services;


use App\Entities\AdminMenu;
use App\Services\Base\BaseService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminMenuService
 * @package App\Http\Services
 */
class AdminMenuService extends BaseService
{
    /**
     * @var MenuGroupService
     */
    private $menuGroupService;

    /**
     * AdminMenuService constructor.
     * @param MenuGroupService $menuGroupService
     */
    public function __construct(MenuGroupService $menuGroupService)
    {
        Parent::__construct();
        $this->menuGroupService = $menuGroupService;
    }

    /**
     * @return Model
     */
    public function model()
    {
        return new AdminMenu();
    }

    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function groupsWithMenus($columns = ['*'])
    {
        return $this->menuGroupService->model->with([
            'adminMenus' => function ($menu) {
                $menu->with('children')->where('parent_id', 0);
            },
        ])->get();
    }
}
