<?php

namespace App\Services;


use App\Services\Base\BaseService;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserService
 * @package App\Http\Services
 */
class UserService extends BaseService
{

    /**
     * @return Model|string
     */
    public function model()
    {
        return new User();
    }

    /**
     * @param $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function create($data)
    {
        $data['password'] = bcrypt($data['password']);

        return $this->model->create($data);
    }

    /**
     * @param array|Model|int $id
     * @param                 $data
     * @return bool|int
     */
    public function update($id, $data)
    {
        $user       = $this->find($id);
        $updateData = array_filter($data);
        if (!empty($updateData['password'])) {
            $updateData['password'] = bcrypt($updateData['password']);
        }

        return $user->update($updateData);
    }
}
