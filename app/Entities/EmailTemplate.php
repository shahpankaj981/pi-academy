<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EmailTemplate
 * @package App\Entities
 */
class EmailTemplate extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'content'];
}
