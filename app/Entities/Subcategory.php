<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Subcategory extends Model
{
    use SyncsWithFirebase;

    protected $fillable = [
        'name',
        'description',
        'order',
        'image',
        'category_id'
    ];
}
