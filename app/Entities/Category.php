<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Category extends Model
{
    use SyncsWithFirebase;

    protected $fillable = [
        'name',
        'description',
        'order',
        'image',
    ];
}
