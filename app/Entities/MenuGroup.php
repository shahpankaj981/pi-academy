<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuGroup
 * @package App\Entities
 */
class MenuGroup extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adminMenus()
    {
        return $this->hasMany(AdminMenu::class, 'group_id');
    }
}
