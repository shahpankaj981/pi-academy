<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminMenu
 * @package App\Entities
 */
class AdminMenu extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        "title",
        "group_id",
        "class",
        "order",
        "icon",
        "is_active",
        "route",
    ];

    /**
     * Menu has many childes menu
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(AdminMenu::class, 'parent_id', 'id');
    }

    /**
     * Menu may belongs to parent menu
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parents()
    {
        return $this->belongsTo(AdminMenu::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(MenuGroup::class, 'group_id');
    }
}
