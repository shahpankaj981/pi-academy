<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('admin_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->unsignedInteger('group_id')->nullable();
            $table->string('title');
            $table->string('class')->nullable();
            $table->string('icon')->nullable();
            $table->string('route');
            $table->boolean('is_active');
            $table->integer('order');
            $table->foreign('group_id')->references('id')->on('menu_groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menus');
        Schema::dropIfExists('menu_groups');
    }
}
