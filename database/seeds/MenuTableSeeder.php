<?php

use App\Constants\MenuGroupConstant;
use App\Services\AdminMenuService;
use App\Services\MenuGroupService;
use Illuminate\Database\Seeder;

/**
 * Class MenuTableSeeder
 */
class MenuTableSeeder extends Seeder
{
    /**
     * @var AdminMenuService
     */
    private $adminMenuService;
    /**
     * @var \App\Services\MenuGroupService
     */
    private $menuGroupService;

    /**
     * MenuTableSeeder constructor.
     * @param AdminMenuService $adminMenuService
     * @param MenuGroupService $menuGroupService
     */
    public function __construct(
        AdminMenuService $adminMenuService,
        MenuGroupService $menuGroupService
    ) {
        $this->adminMenuService = $adminMenuService;
        $this->menuGroupService = $menuGroupService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $menus = [
            [
                "title"     => "User",
                "class"     => "nav-item",
                "order"     => 1,
                "icon"      => "m-menu__link-icon flaticon-user",
                "is_active" => true,
                "route"     => "user.index",
                "group_id"  => MenuGroupConstant::GENERAL,
                "children"  => [
                    [
                        "title"     => "All",
                        "class"     => "nav-item",
                        "order"     => 1,
                        "icon"      => "<i class='fa fa-users'></i>",
                        "is_active" => true,
                        "route"     => "user.index",
                    ],
                    [
                        "title"     => "Create User",
                        "class"     => "nav-item",
                        "order"     => 2,
                        "icon"      => "<i class='fa fa-plus'></i>",
                        "is_active" => true,
                        "route"     => "user.create",
                    ],
                ],
            ],
            [
                "title"     => "Category",
                "class"     => "nav-item",
                "order"     => 1,
                "icon"      => "m-menu__link-icon flaticon-settings",
                "is_active" => true,
                "route"     => "category.index",
                "group_id"  => MenuGroupConstant::GENERAL,
                "children"  => [
                    [
                        "title"     => "All",
                        "class"     => "nav-item",
                        "order"     => 1,
                        "icon"      => "<i class='fa fa-envelope-open'></i>",
                        "is_active" => true,
                        "route"     => "category.index",
                    ],
                    [
                        "title"     => "Create Category",
                        "class"     => "nav-item",
                        "order"     => 2,
                        "icon"      => "<i class='fa fa-plus'></i>",
                        "is_active" => true,
                        "route"     => "category.create",
                    ],
                ],
            ],
            [
                "title"     => "SubCategory",
                "class"     => "nav-item",
                "order"     => 1,
                "icon"      => "m-menu__link-icon flaticon-settings",
                "is_active" => true,
                "route"     => "subcategory.index",
                "group_id"  => MenuGroupConstant::GENERAL,
                "children"  => [
                    [
                        "title"     => "All",
                        "class"     => "nav-item",
                        "order"     => 1,
                        "icon"      => "<i class='fa fa-envelope-open'></i>",
                        "is_active" => true,
                        "route"     => "subcategory.index",
                    ],
                    [
                        "title"     => "Create SubCategory",
                        "class"     => "nav-item",
                        "order"     => 2,
                        "icon"      => "<i class='fa fa-plus'></i>",
                        "is_active" => true,
                        "route"     => "subcategory.create",
                    ],
                ],
            ],
        ];

        $groups = [
            [
                'title' => 'General',
                'order' => 1,
            ],
            [
                'title' => 'Master Entry',
                'order' => 2,
            ],
        ];
        foreach ($groups as $group) {
            $this->menuGroupService->updateOrCreate(['title' => $group['title']], $group);
        }
        $this->adminMenuService->truncate();

        foreach ($menus as $menu) {
            $childrenMenus = $menu['children'];
            unset($menu['children']);
            $parentMenu = $this->adminMenuService->create($menu);
            foreach ($childrenMenus as $childrenMenu) {
                $childrenMenu['parent_id'] = $parentMenu->id;
                $this->adminMenuService->updateOrCreate($childrenMenu, $childrenMenu);
            }
        }
    }
}
